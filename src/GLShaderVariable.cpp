//
// Created by znix on 11/15/17.
//

#include "GLShaderVariable.h"

GLShaderVariable::GLShaderVariable(GLint id) : varId(id) {

}

void GLShaderVariable::UniformMatrix4(const GLfloat *data, bool transpose) {
	glUniformMatrix4fv(varId, 1, transpose ? GL_TRUE : GL_FALSE, data);
}

void GLShaderVariable::Uniform3f(GLfloat x, GLfloat y, GLfloat z) {
	glUniform3f(varId, x, y, z);
}
