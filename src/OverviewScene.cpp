//
// Created by znix on 11/15/17.
//

#include <glm/detail/type_mat.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "OverviewScene.h"

OverviewScene::OverviewScene(ShaderManager &shaders, GLFWwindow *win) : Scene(shaders, win) {
	interiorShader = shaders.Load("interior");
	exteriorShader = shaders.Load("colour");

	GLuint vaos[2];
	glGenVertexArrays(2, vaos);
	interiorVAO = vaos[0];
	exteriorVAO = vaos[1];

	glBindVertexArray(interiorVAO);

	// An array of 3 vectors which represents 3 vertices
	static const GLfloat g_vertex_buffer_data[] = {
			-1.0f, -1.0f, 0.0f,
			1.0f, -1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,

			1.0f, -1.0f, 0.0f,
			3.0f, -1.0f, 0.0f,
			2.0f, 1.0f, 0.0f,
	};

	// Generate 1 buffer, put the resulting identifier in vertexbuffer
	glGenBuffers(1, &vertexbuffer);
	// The following commands will talk about our 'vertexbuffer' buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	// Give our vertices to OpenGL.
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);

	// 1rst attribute buffer : vertices
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
			0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void *) 0          // array buffer offset
	);
}

void OverviewScene::Render() {
	Scene::PreRender();

	glBindVertexArray(interiorVAO);

	// TODO move to constructor
	// Get a handle for our "MVP" uniform
	// Only during the initialisation
	auto shaderMVP = interiorShader->GetUniformLocation("MVP");
	auto shaderMVP2 = exteriorShader->GetUniformLocation("MVP");
	auto colour = interiorShader->GetUniformLocation("cin");


	// Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	// glm::mat4 Projection = glm::perspective(glm::radians(45.0f), (float) width / (float) height, 0.1f, 100.0f);

	// Or, for an ortho camera :
	glm::mat4 Projection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, 0.0f, 100.0f); // In world coordinates

	// Camera matrix
	glm::mat4 View = glm::lookAt(
			glm::vec3(4, 3, 3), // Camera is at (4,3,3), in World Space
			glm::vec3(4, 3, 0), // and looks at the origin
			glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
	);

	// Model matrix : an identity matrix (model will be at the origin)
	glm::mat4 Model = glm::mat4(1.0f);
	// Our ModelViewProjection : multiplication of our 3 matrices
	glm::mat4 mvp = Projection * View * Model; // Remember, matrix multiplication is the other way around

	interiorShader->Use();
	shaderMVP->UniformMatrix4(&mvp[0][0]);

	// Send our transformation to the currently bound shader, in the "MVP" uniform
	// This is done in the main loop since each model will have a different MVP matrix (At least for the M part)

	// Draw the triangle!
	glDrawArrays(GL_TRIANGLES, 0, 3); // Starting from vertex 0; 3 vertices total -> 1 triangle

	exteriorShader->Use();
	shaderMVP2->UniformMatrix4(&mvp[0][0]);
	colour->Uniform3f(1, 1, 0);
	glDrawArrays(GL_TRIANGLES, 3, 3); // Starting from vertex 0; 3 vertices total -> 1 triangle
}
