//
// Created by znix on 11/15/17.
//

#include "Shader.h"
#include "cpptoml.h"
#include <iostream>
#include <GLShaderVariable.h>

using namespace std;

static GLuint LoadAndCompileShader(string filename, GLenum type);

Shader::Shader(string name) {
	std::cout << "Loading shader " << name << std::endl;
	auto config = cpptoml::parse_file("data/shaders/" + name + ".toml");
	auto frag = config->get_as<string>("fragment");
	auto vert = config->get_as<string>("vertex");

	if (!frag) {
		throw "Shader '" + name + "' missing fragment definition";
	}
	if (!vert) {
		throw "Shader '" + name + "' missing vertex definition";
	}

	std::cout << "Using fragment shader " << *frag << std::endl;

	// Create the shaders
	GLuint VertexShaderID = LoadAndCompileShader("data/shaders/" + *vert + ".vert", GL_VERTEX_SHADER);
	GLuint FragmentShaderID = LoadAndCompileShader("data/shaders/" + *frag + ".frag", GL_FRAGMENT_SHADER);

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Link the program
	cout << "Linking program\n" << endl;
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);

	
	// glDetachShader(ProgramID, VertexShaderID);
	// glDetachShader(ProgramID, FragmentShaderID);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	shaderId = ProgramID;

	if (InfoLogLength > 0) {
		char* str = (char*)std::malloc(InfoLogLength);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, str);
		cout << str << endl;

		this->~Shader(); // Clean up
		std::exit(1);
 	}
}

Shader::~Shader() {
	glDeleteProgram(shaderId);
}

void Shader::Use() {
	glUseProgram(shaderId);
}

shared_ptr<GLShaderVariable> Shader::GetUniformLocation(string name) {
	GLint id = glGetUniformLocation(shaderId, name.c_str());
	return shared_ptr<GLShaderVariable>(new GLShaderVariable(id));
}

static GLuint LoadAndCompileShader(string filename, GLenum type) {
	GLuint id = glCreateShader(type);

	// Read the Vertex Shader code from the file
	std::ifstream in(filename, std::ios::in);
	if (!in.is_open()) {
		cerr << "Impossible to open " << filename << endl;
		return 0;
	}

	std::ostringstream contents;
	contents << in.rdbuf();
	in.close();
	string source = contents.str();


	cout << "Compiling shader: " << filename << endl;
	char const *SrcPtr = source.c_str();
	glShaderSource(id, 1, &SrcPtr, NULL);
	glCompileShader(id);


	// Check Vertex Shader
	GLint InfoLogLength;
	glGetShaderiv(id, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		char* str = (char*)std::malloc(InfoLogLength);
	
		glGetShaderInfoLog(id, InfoLogLength, NULL, str);
		cout << str << endl;
		std::exit(1);
	}
	
	return id;
}
