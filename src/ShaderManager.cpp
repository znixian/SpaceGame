//
// Created by znix on 11/15/17.
//

#include "ShaderManager.h"

using namespace std;

shared_ptr<Shader> ShaderManager::Load(string name) {
	// Check if we've already loaded that shader
	auto current = shaders[name];
	if (!current.expired()) {
		return current.lock();
	}

	// Otherwise create a new one
	shared_ptr<Shader> shader(new Shader(name));
	shaders[name] = shader;
	return shader;
}

