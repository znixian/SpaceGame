#include<unistd.h>
#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/mat4x4.hpp>
#include <Shader.h>
#include <TestScene.h>
#include <OverviewScene.h>

using namespace std;

int main() {
	try {
		/* Initialize the library */
		if (!glfwInit())
			return -1;

		int width = 640, height = 480;

		// explicity request a 3.* core profile (required for older
		// machines, like mine)
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		/* Create a windowed mode window and its OpenGL context */
		GLFWwindow *window = glfwCreateWindow(width, height, "Hello World", NULL, NULL);
		if (!window) {
			glfwTerminate();
			return -1;
		}
		/* Make the window's context current */

		glfwMakeContextCurrent(window);

		glewExperimental = true; // Needed for core profile
		if (glewInit() != GLEW_OK) {
			fprintf(stderr, "Failed to initialize GLEW\n");
			getchar();
			glfwTerminate();
			return -1;
		}


		ShaderManager shaders;

		TestScene test(shaders, window);
		test.SetViewport(0, 0.5, 0.5, 0.5);

		OverviewScene overview(shaders, window);
		overview.SetViewport(0, 0, 0.5, 0.5);

		/* Loop until the user closes the window */
		while (!glfwWindowShouldClose(window)) {
			// Enable depth test
			glEnable(GL_DEPTH_TEST);
			// Accept fragment if it closer to the camera than the former one
			glDepthFunc(GL_LESS);

			// Clear the screen
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			// Render the scene
			test.Render();
			overview.Render();

			/* Swap front and back buffers */
			glfwSwapBuffers(window);

			/* Poll for and process events */
			glfwPollEvents();
		}
		glfwDestroyWindow(window);
		glfwTerminate();

	} catch (std::string msg) {
		cout << "Exception: " << msg << endl;
	}

	return 0;
}
