//
// Created by znix on 11/15/17.
//

#include "Scene.h"

void Scene::PreRender() {
	// Find the window size
	glfwGetWindowSize(window, &windowWidth, &windowHeight);

	// Set the viewport
	glViewport(
			(GLint) (viewportX * windowWidth),
			(GLint) (viewportY * windowHeight),
			(GLint) (viewportWidth * windowWidth),
			(GLint) (viewportHeight * windowHeight)
	);

	width = (int) (viewportWidth * windowWidth);
	height = (int) (viewportHeight * windowHeight);
}

// Getters and setters
