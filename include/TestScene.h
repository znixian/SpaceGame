/**
 * A testing scene displaying a single yellow 3D triangle.
 *
 * Created by znix on 11/15/17.
 */
#include <memory>
#include <GLFW/glfw3.h>
#include "Shader.h"
#include "ShaderManager.h"
#include "Scene.h"

#pragma once

class TestScene : public Scene {
public:
	TestScene(ShaderManager &shaders, GLFWwindow *win);

	virtual void Render();

private:
	std::shared_ptr<Shader> shader;
	GLuint vertexbuffer;
	GLuint VertexArrayID;
};

