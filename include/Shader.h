/**
 * Represents a single GLSL program (vertex plus
 * fragment shaders).
 *
 * Note that you cannot instanciate this class
 * directly; See ShaderManager::Load
 *
 * FIXME both shaders will be loaded, even if they have
 * previously been used in another program.
 *
 * Created by znix on 11/15/17.
 */

#pragma once

#include <string>
#include <GL/glew.h>
#include <memory>
#include "GLShaderVariable.h"

class ShaderManager;

class Shader {
public:
	~Shader();

	void Use();

	std::shared_ptr<GLShaderVariable> GetUniformLocation(std::string name);

private:
	GLuint shaderId;

	Shader(std::string name);

	friend class ShaderManager;
};
