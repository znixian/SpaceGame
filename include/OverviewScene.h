/**
 * A scene to show the interior of a spacecraft.
 * If it is showing the player's spacecraft, the user
 * will also be able to interact with it.
 *
 * Created by znix on 11/15/17.
 */

#pragma once

#include "Scene.h"

class OverviewScene : public Scene {
public:
	OverviewScene(ShaderManager &shaders, GLFWwindow *win);

	virtual void Render();

private:
	std::shared_ptr<Shader> interiorShader;
	std::shared_ptr<Shader> exteriorShader;
	GLuint vertexbuffer;
	GLuint interiorVAO;
	GLuint exteriorVAO;
};
