/**
 * A scene that can be positioned on the screen,
 * and then drawn in the main loop, although they
 * could probably also be nested.
 *
 * Created by znix on 11/15/17.
 */
#include <memory>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "Shader.h"
#include "ShaderManager.h"

#pragma once

class Scene {
public:
	Scene(ShaderManager &shaders, GLFWwindow *win) : window(win) {}

	virtual void Render() = 0;


	// Viewport getters/setters
	void SetViewport(float viewportX, float viewportY, float viewportWidth, float viewportHeight) {
		this->viewportX = viewportX;
		this->viewportY = viewportY;
		this->viewportWidth = viewportWidth;
		this->viewportHeight = viewportHeight;
	}

protected:
	GLFWwindow *window;
	int windowWidth, windowHeight, width, height;
	float viewportX = 0, viewportY = 0, viewportWidth = 1, viewportHeight = 1; // Viewport xy,wh

	void PreRender();

};

