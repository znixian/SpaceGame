/**
 * A
 *
 * Created by znix on 11/15/17.
 */

#pragma once

#include "Shader.h"
#include <memory>
#include <map>

class ShaderManager {
public:
	std::shared_ptr<Shader> Load(std::string name);

private:
	std::map<std::string, std::weak_ptr<Shader>> shaders;
};
