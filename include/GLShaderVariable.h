/**
 * Represents a Uniform GLSL variable, presenting a
 * C++ interface.
 *
 * Created by znix on 11/15/17.
 */

#pragma once

#include <GL/glew.h>

class Shader;

class GLShaderVariable {
public:
	void Uniform3f(GLfloat x, GLfloat y, GLfloat z);

	void UniformMatrix4(const GLfloat *data, bool transpose = false);

private:
	GLShaderVariable(GLint id);

	GLint varId;

	friend class Shader;
};

